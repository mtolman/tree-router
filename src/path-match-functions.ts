import { is } from '@tofurama3000/hat-js';
import { extractReturn } from './extraction-functions';
import { collectPathParams, stitchNewPath, nextTreeNode } from './helper-functions';

const { isFunction } = is;

export function exactPathMatch(path, middleware = undefined) {
  return str => ({
    matches: str.substr(0, path.length) === path,
    params: {},
    newPath: str.substr(path.length),
    middleware
  });
}

export function pathParamMatch(segmentPath, middleware = undefined) {
  const segmentPathParts = segmentPath.split('/');
  return fullPath => {
    const fullPathParts = fullPath.split('/');
    const params = collectPathParams(fullPathParts, segmentPathParts);
    return params
      ? {
          matches: true,
          params,
          newPath: stitchNewPath(fullPathParts.slice(segmentPathParts.length)),
          middleware
        }
      : { matches: false, params: {}, newPath: '', middleware: [] };
  };
}

export function matchPath(tree, path, params = {}, middleware = []) {
  if (!tree.canMoveUp() || tree.finished) {
    return null;
  }

  let curZipper = tree;

  while (curZipper && curZipper.canMoveDown()) {
    curZipper = curZipper.moveDown();

    // Try to check this path
    const treeLevelMatch = matchCurrentTreeLevel(curZipper, path, params, middleware);
    if (treeLevelMatch) {
      return treeLevelMatch;
    }

    curZipper = nextTreeNode(curZipper);
  }

  return null;
}

function matchCurrentTreeLevel(curZipper, path, params, middleware) {
  if (isFunction(curZipper.nodeRaw())) {
    const result = curZipper.nodeRaw()(path);
    const newParams = { ...params, ...result.params };
    const newMiddleware = middleware.concat(result.middleware);
    if (result.matches) {
      if (!result.newPath) {
        const obj = extractReturn(curZipper);
        if (obj) {
          return { result: obj, params: newParams, middleware: newMiddleware };
        }
      }
      if (result.newPath) {
        // Check all of the nested paths
        while (curZipper.canMoveRight()) {
          curZipper = curZipper.moveRight();
          const match = matchPath(curZipper, result.newPath, newParams, newMiddleware);
          if (match) {
            return match;
          }
        }
      }
    }
  }
  return null;
}

export const _matchCurrentTreeLevel = matchCurrentTreeLevel;
