import { is, immutable } from '@tofurama3000/hat-js';
import { buildPathTree } from './path-tree';
import { searchTree } from './searchTree';
import { matchPath } from './path-match-functions';
import { Predicate } from '@tofurama3000/hat-js/commonTypes';

const { isObject } = is;
const createZipper = immutable.Zipper.createZipper;

export type RouteData = Array<string | any | RouteData>;

export interface Router {
  match: (
    path: string
  ) => { result: any; params: { [paramName: string]: string }; middleware: Function[] };
  lookup: (predicate: Predicate<any>) => string;
  warnings: string[];
}

function router(data: RouteData): Router {
  const zipper = createZipper(data);
  const { tree, warnings } = buildPathTree(data);
  return {
    match: function(path) {
      if (!tree.canMoveDown()) {
        return null;
      }
      if (path.length > 1 && path[path.length - 1] === '/') {
        path = path.slice(0, -1);
      }
      return matchPath(tree.moveDown(), path);
    },
    lookup: function(matchCondition) {
      if (!zipper.canMoveDown()) {
        return null;
      }
      return searchTree(zipper.moveDown(), matchCondition);
    },
    warnings
  };
}

router.router = router;
router.nameIs = name => node => isObject(node) && (node as any).name === name;

export default router;
