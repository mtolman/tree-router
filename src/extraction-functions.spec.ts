import { createZipper } from '@tofurama3000/hat-js/immutable/zipper/index';
import { extractReturn, extractMiddleware } from './extraction-functions';

// Extraction functions ignore first element since that is reserved for the path
describe('extractReturn', () => {
  it('Extracts first non-nested list from a tier', () => {
    const zipper = createZipper(['ignored', [1, 2], [3, 4], 5, [6, 7]]);
    expect(extractReturn(zipper.moveDown())).toBe(5);
    expect(extractReturn(createZipper(['ignored', 0, [1, 2], [3, 4], 5, [6, 7]]).moveDown())).toBe(
      0
    );
  });

  it('Returns null if there is no non-nested list from a tier', () => {
    expect(extractReturn(createZipper(['ignored', [1, 2], [3, 4], [6, 7]]).moveDown())).toBeNull();
  });
});

describe('extractMiddleware', () => {
  it('Extracts middleware from first non-nested list from a tier', () => {
    expect(
      extractMiddleware(
        createZipper(['ignored', [1, 2], [3, 4], { middleware: [5] }, [6, 7]]).moveDown()
      )
    ).toEqual([5]);
    expect(
      extractMiddleware(
        createZipper([
          'ignored',
          { middleware: [0] },
          [1, 2],
          [3, 4],
          { middleware: 5 },
          [6, 7]
        ]).moveDown()
      )
    ).toEqual([0]);
  });

  it('Returns [] if there is no non-nested list from a tier', () => {
    expect(extractMiddleware(createZipper(['ignored', [1, 2], [3, 4], [6, 7]]).moveDown())).toEqual(
      []
    );
  });

  it('Returns [] if there is no middleware in first non-nested list from a tier', () => {
    expect(
      extractMiddleware(createZipper(['ignored', [1, 2], {}, [3, 4], [6, 7]]).moveDown())
    ).toEqual([]);
    expect(
      extractMiddleware(
        createZipper(['ignored', 3, [1, 2], { middleware: [4] }, [3, 4], [6, 7]]).moveDown()
      )
    ).toEqual([]);
  });
});
