import { is } from '@tofurama3000/hat-js';
const { isObject, isArray } = is;

export function extractMiddleware(zipper) {
  while (zipper.canMoveRight()) {
    zipper = zipper.moveRight();
    if (!isArray(zipper.nodeRaw())) {
      if (isObject(zipper.nodeRaw())) {
        const obj = zipper.node();
        if (obj.middleware && isArray(obj.middleware)) {
          return obj.middleware;
        }
      }
      break;
    }
  }
  return [];
}

export function extractReturn(zipper) {
  while (zipper.canMoveRight()) {
    zipper = zipper.moveRight();
    if (!isArray(zipper.nodeRaw())) {
      return zipper.node();
    }
  }
  return null;
}
