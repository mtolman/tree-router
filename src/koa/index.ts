import { is } from '@tofurama3000/hat-js';
import router from '../index';
import { Context, Middleware } from 'koa';
import { IncomingHttpHeaders } from 'http';
const { isObject, isArray, isString } = is;

export interface Response {
  status?: number;
  body?: Promise<any> | string | any;
  headers?: IncomingHttpHeaders;
}

type HandlerFn = (
  res: {
    pathParams: { [key: string]: string };
    query: { [key: string]: string };
    body: any;
    headers: IncomingHttpHeaders;
  },
  ctx?: Context
) => Response;

export interface Handler {
  parameters: { query: string[] };
  middleware: Array<Middleware>;
  handler: HandlerFn;
}

export type RouteTree = Array<
  | string
  | {
      get?: Handler;
      post?: Handler;
      put?: Handler;
      delete?: Handler;
      patch?: Handler;
      head?: Handler;
      options?: Handler;
      trace?: Handler;
      any?: Handler;
      middleware?: Array<Middleware>;
    }
  | RouteTree
>;

export default (
  data: RouteTree[],
  {
    notFoundHandler = (ctx, next) => {
      ctx.response.status = 404;
      ctx.response.body = '<html><body>Page Not Found</body></html>';
      return next();
    },
    serverErrorHandler = (ctx, next) => {
      ctx.response.status = 500;
      ctx.response.body = '<html><body>Internal Server Error</body></html>';
      return next();
    },
    errLog = console.error,
    reverseMiddleware = false
  }: {
    notFoundHandler?: Middleware;
    serverErrorHandler?: Middleware;
    errLog?: (_: any) => void;
    reverseMiddleware?: boolean;
  } = {}
) => {
  const r = router(data);
  return async (ctx: Context, next) => {
    const method = ctx.request.method.toLowerCase();
    const url = ctx.request.url;
    const urlParts = url.split('?');
    const uri = urlParts[0];
    const query = (urlParts[1] || '')
      .split('&')
      .filter(x => x)
      .map(x => x.split('='))
      .reduce((acc, [key, value]) => {
        if (key in acc) {
          if (!isArray(acc[key])) {
            acc[key] = [acc[key]];
          }
          acc[key].push(value);
        } else {
          acc[key] = value;
        }
        return acc;
      }, {});
    const match = r.match(uri);

    if (match && isObject(match.result) && isObject(match.result[method] || match.result['any'])) {
      const methodMatch = match.result[method] || match.result['any'];
      let middleware = (methodMatch.middleware || []).concat(match.middleware);

      if (reverseMiddleware) {
        middleware = middleware.reverse();
      }

      const executionPlan = middleware.reduce(
        (nextFn, curFn) => () => curFn(ctx, nextFn),
        processRequest(methodMatch, match.params, query, ctx, next, errLog)
      );
      return executionPlan()
        .catch(e => {
          console.error(e);
          throw e;
        })
        .catch(() => serverErrorHandler(ctx, next));
    } else {
      return notFoundHandler(ctx, next);
    }
  };
};

function processRequest(
  methodMatch: { handler: HandlerFn },
  pathParams: { [key: string]: string },
  query: { [key: string]: string },
  ctx: Context,
  next: () => any,
  errLog: (_: any) => void
) {
  return () => {
    const handler = methodMatch.handler;

    const firstParam = {
      query,
      pathParams,
      body: (ctx.request as any).body || {},
      headers: ctx.headers
    };

    if (handler.length === 1) {
      return Promise.resolve(handler(firstParam))
        .then(async (response: Response) => {
          ctx.status = response.status || 200;
          Object.entries(response.headers || {}).forEach(([key, value]) => ctx.set(key, value));
          const body = await Promise.resolve(response.body);
          if (!isString(body)) {
            ctx.set('content-type', 'application/json');
            ctx.body = JSON.stringify(body);
          } else {
            ctx.set('content-type', 'text/html');
            ctx.body = body;
          }
        })
        .catch(err => {
          errLog(err);
          ctx.status = 500;
          ctx.body = 'Internal Server Error';
        })
        .then(() => next());
    } else {
      return Promise.resolve(handler(firstParam, ctx)).then(() => next());
    }
  };
}
