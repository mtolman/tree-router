import { collectPathParams, stitchNewPath, nextTreeNode } from './helper-functions';
import { createZipper } from '@tofurama3000/hat-js/immutable/zipper';

describe('collectPathParams', () => {
  it('Terminates early if there are too few fullPath pieces', () => {
    expect(collectPathParams([], ['a'])).toBeNull();
  });

  it('Performs exact matches', () => {
    expect(collectPathParams(['a', 'b', 'c'], ['a', 'b', 'c'])).toEqual({});
  });

  it('Collects parameters', () => {
    expect(collectPathParams(['a', 'b', 'c'], [':alpha', ':beta', ':omega'])).toEqual({
      alpha: 'a',
      beta: 'b',
      omega: 'c'
    });
  });

  it('Returns null on a failure', () => {
    expect(collectPathParams(['a', 'b', 'c'], ['a', 'beta', 'c'])).toBeNull();
    expect(collectPathParams(['a', 'b', 'c'], [':alpha', ':beta', 'omega'])).toBeNull();
  });
});

describe('stitchNewPath', () => {
  it('Returns an empty string when given an empty array', () => {
    expect(stitchNewPath([])).toBe('');
  });

  it('Adds a leading slash', () => {
    expect(stitchNewPath(['a'])).toBe('/a');
  });

  it('Adds middle slashes', () => {
    expect(stitchNewPath(['a', 'b'])).toBe('/a/b');
  });
});

describe('nextTreeNode', () => {
  it('selects the right node', () => {
    const zipper = createZipper([
      ['/a', ['/b/c', 5], ['/c/d', 4]],
      ['/e', ['/f/g', 6]],
      12,
      ['/h', ['/i/j', ['/k', 12]]]
    ]).moveDown();

    expect(nextTreeNode(zipper.moveDown()).toArray()).toEqual(['/e', ['/f/g', 6]]);

    expect(nextTreeNode(zipper.moveRight().moveDown()).toArray()).toEqual([
      '/h',
      ['/i/j', ['/k', 12]]
    ]);
    expect(
      nextTreeNode(
        zipper
          .moveRight()
          .moveRight()
          .moveRight()
          .moveDown()
      )
    ).toBeNull();
    expect(nextTreeNode(zipper)).toBeNull();
  });
});
