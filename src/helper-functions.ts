export function collectPathParams(fullPathParts, segmentPathParts) {
  // Our full path can't possibly match the segment path since it is smaller
  if (fullPathParts.length < segmentPathParts.length) {
    return null;
  }

  const params = {};
  for (let i = 0; i < segmentPathParts.length; ++i) {
    const fullPathPiece = segmentPathParts[i];
    const segmentPathPiece = fullPathParts[i];

    if (fullPathPiece.length && fullPathPiece[0] === ':') {
      params[fullPathPiece.substr(1)] = segmentPathPiece;
    } else if (fullPathPiece !== segmentPathPiece) {
      return null;
    } // else continue
  }
  return params;
}

export function stitchNewPath(pathParts) {
  return ['', ...pathParts].join('/');
}

export function nextTreeNode(curZipper) {
  // Move to the next node (since we went down, we have to go back up and then over)
  if (curZipper.canMoveUp() && curZipper.moveUp().canMoveRight()) {
    curZipper = curZipper.moveUp().moveRight();

    // Keep going right until we hit a new sub-tree
    while (!curZipper.canMoveDown() && curZipper.canMoveRight()) {
      curZipper = curZipper.moveRight();
    }
    return curZipper;
  } else {
    // we couldn't find the next node, so break
    return null;
  }
}
