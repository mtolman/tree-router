import { is } from '@tofurama3000/hat-js';
import { extractReturn } from './extraction-functions';

const { isString } = is;

export function searchTree(tree, matchCond) {
  while (true) {
    if (!tree.canMoveDown()) {
      if (tree.canMoveRight()) {
        tree = tree.moveRight();
      } else {
        break;
      }
    } else {
      tree = tree.moveDown();

      if (isString(tree.nodeRaw())) {
        const curPath = tree.node();
        const ret = extractReturn(tree);
        if (ret && matchCond(ret)) {
          return curPath;
        } else {
          while (tree.canMoveRight()) {
            tree = tree.moveRight();
            const subSearch = searchTree(tree, matchCond);
            if (subSearch) {
              return curPath + subSearch;
            }
          }
        }
      }

      tree = tree.moveUp();
      if (tree.canMoveRight()) {
        tree = tree.moveRight();
      } else {
        break;
      }
    }
  }
  return null;
}
