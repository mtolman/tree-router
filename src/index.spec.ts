import router from './index';

describe('Router', () => {
  it('Takes valid input', () => {
    expect(
      router([
        [
          '/api',
          ['/v1', { name: 'api-v1' }],
          ['/v2', ['/:endpoint', { name: 'api-v2-endpoint' }]],
          ['/v3', ['/status', 'ok'], ['/:endpoint/api', { name: 'api-v3-endpoint' }]]
        ]
      ])
    ).toBeTruthy();
  });

  it('Can match basic paths', () => {
    const routes = router([
      [
        '/api',
        ['/v1', { name: 'api-v1' }],
        ['/v2', ['/:endpoint', { name: 'api-v2-endpoint' }]],
        ['/v3', ['/status', 'ok'], ['/:endpoint/api', { name: 'api-v3-endpoint' }]]
      ]
    ]);

    expect(routes.match('/api/v1')).toEqual({
      result: { name: 'api-v1' },
      params: {},
      middleware: []
    });
    expect(routes.match('/api/v1/')).toEqual({
      result: { name: 'api-v1' },
      params: {},
      middleware: []
    });
    expect(routes.match('/api/v3/status')).toEqual({ result: 'ok', params: {}, middleware: [] });
    expect(routes.match('/api/v3/status/api')).toEqual({
      result: { name: 'api-v3-endpoint' },
      params: { endpoint: 'status' },
      middleware: []
    });
    expect(routes.match('/api/v2/customer')).toEqual({
      result: { name: 'api-v2-endpoint' },
      params: { endpoint: 'customer' },
      middleware: []
    });
  });

  it('handles empty trees', () => {
    const routes = router([]);
    expect(routes.match('/api/v2/customer')).toBeNull();
    expect(routes.lookup(_ => true)).toBeNull();
  });

  it('Can handle middleware', () => {
    const routes = router([
      [
        '/api',
        { middleware: ['0'] },
        ['/v1', { name: 'api-v1', middleware: ['1', '2', '3'] }],
        ['/v2', ['/:endpoint', { name: 'api-v2-endpoint', middleware: ['4'] }]],
        [
          '/v3',
          ['/status', 'ok'],
          ['/:endpoint/api', { name: 'api-v3-endpoint', middleware: ['6'] }],
          { middleware: ['5'] }
        ]
      ]
    ]);

    expect(routes.match('/api/v1')).toEqual({
      result: { name: 'api-v1', middleware: ['1', '2', '3'] },
      params: {},
      middleware: ['0', '1', '2', '3']
    });
    expect(routes.match('/api/v1/')).toEqual({
      result: { name: 'api-v1', middleware: ['1', '2', '3'] },
      params: {},
      middleware: ['0', '1', '2', '3']
    });
    expect(routes.match('/api/v3/status')).toEqual({
      result: 'ok',
      params: {},
      middleware: ['0', '5']
    });
    expect(routes.match('/api/v3/status/api')).toEqual({
      result: { name: 'api-v3-endpoint', middleware: ['6'] },
      params: { endpoint: 'status' },
      middleware: ['0', '5', '6']
    });
    expect(routes.match('/api/v2/customer')).toEqual({
      result: { name: 'api-v2-endpoint', middleware: ['4'] },
      params: { endpoint: 'customer' },
      middleware: ['0', '4']
    });
  });

  it('Can handle nested path params', () => {
    const routes = router([['/api', ['/:api-version', ['/:endpoint', { name: 'endpoint' }]]]]);

    expect(routes.match('/api/v1')).toEqual(null);
    expect(routes.match('/api/v1/')).toEqual(null);
    expect(routes.match('/api/v3/status')).toEqual({
      result: { name: 'endpoint' },
      params: { 'api-version': 'v3', endpoint: 'status' },
      middleware: []
    });
    expect(routes.match('/api/v3/status/api')).toEqual(null);
    expect(routes.match('/api/v2/customer')).toEqual({
      result: { name: 'endpoint' },
      params: { 'api-version': 'v2', endpoint: 'customer' },
      middleware: []
    });
  });

  it('Can reverse match basic paths', () => {
    const routes = router([
      [
        '/api',
        ['/v1', { name: 'api-v1' }],
        ['/v2', ['/:endpoint', { name: 'api-v2-endpoint' }]],
        ['/v3', ['/status', 'ok'], ['/:endpoint/api', { name: 'api-v3-endpoint' }]]
      ]
    ]);

    expect(routes.lookup(router.nameIs('api-v1'))).toEqual('/api/v1');
    expect(routes.lookup(router.nameIs('api-v2-endpoint'))).toEqual('/api/v2/:endpoint');
    expect(routes.lookup(n => n === 'ok')).toEqual('/api/v3/status');
  });
});
