import { searchTree } from './searchTree';
import { createZipper } from '@tofurama3000/hat-js/immutable/zipper/index';

describe('buildPathTree', () => {
  it('Handles empty trees', () => {
    searchTree(createZipper([]), _ => true);
    searchTree(createZipper([['/api', 'a', ['/1'], ['/1'], ['/1'], ['/1']]]), _ => false);
    searchTree(
      createZipper([['/api', 'a', ['/1'], ['/1'], ['/1'], ['/1']]])
        .moveDown()
        .moveDown(),
      _ => false
    );
  });
});
