import { is, iterables, immutable } from '@tofurama3000/hat-js';
import { exactPathMatch, pathParamMatch } from './path-match-functions';
import { extractMiddleware } from './extraction-functions';

const { any } = iterables;
const { isString } = is;
const createZipper = immutable.Zipper.createZipper;

export function buildPathTree(data) {
  let dataZipper = createZipper(data);
  const warnings = [];

  if (dataZipper.canMoveDown()) {
    dataZipper = dataZipper.moveDown();

    while (true) {
      // We want to be as forgiving as possible, so we keep track of whether or not we can process the current subtree
      let canProcessSubtree = false;

      // move into a subtree
      if (dataZipper.canMoveDown()) {
        dataZipper = dataZipper.moveDown();
        canProcessSubtree = true;
      }

      // process subtree
      if (canProcessSubtree) {
        if (!isString(dataZipper.nodeRaw())) {
          warnings.push('invalid path string at ' + JSON.stringify(dataZipper.path));
        } else {
          const path = dataZipper.nodeRaw();
          const parts = path.split('/');
          if (any(part => part.substr(0, 1) === ':', parts)) {
            dataZipper = dataZipper.change(pathParamMatch(path, extractMiddleware(dataZipper)));
          } else {
            dataZipper = dataZipper.change(exactPathMatch(path, extractMiddleware(dataZipper)));
          }
        }
      }

      // move to next subtree
      if (dataZipper.canMoveRight()) {
        dataZipper = dataZipper.moveRight();
      } else {
        while (dataZipper.canMoveUp() && !dataZipper.canMoveRight()) {
          dataZipper = dataZipper.moveUp();
        }
        if (!dataZipper.canMoveRight()) {
          break;
        }
        dataZipper = dataZipper.moveRight();
      }
    }
  }

  return { tree: dataZipper, warnings };
}
