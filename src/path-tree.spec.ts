import { buildPathTree } from './path-tree';

describe('buildPathTree', () => {
  it('Tests warnings for invalid paths', () => {
    const { warnings } = buildPathTree([['/api', [5, 5], [{ foo: 'bar' }]]]);
    expect(warnings).toEqual([
      'invalid path string at {"left":[],"up":{"left":[null,[]],"up":{"left":[],"right":[]},"right":[[{"foo":"bar"},[]],[]]},"right":[5,[]]}',
      'invalid path string at {"left":[],"up":{"left":[[5,[5,[]]],[null,[]]],"up":{"left":[],"right":[]},"right":[]},"right":[]}'
    ]);
  });
});
