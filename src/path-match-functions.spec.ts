import { exactPathMatch, pathParamMatch, matchPath } from './path-match-functions';
import { createZipper } from '@tofurama3000/hat-js/immutable/zipper/index';

describe('exactPathMatch', () => {
  it('Performs exact path matching on a substring', () => {
    const matcher = exactPathMatch('/a/b/c');
    expect(matcher('/a/b').matches).toBe(false);
    expect(matcher('/a/beta/c').matches).toBe(false);
    expect(matcher('/a/b/c/d').matches).toBe(true);
    expect(matcher('/a/b/ceta').matches).toBe(true);
  });
});

describe('pathParamMatch', () => {
  it('Performs path param path matching on a substring', () => {
    const matcher = pathParamMatch('/a/:b/c');
    expect(matcher('/a/b').matches).toBe(false);
    expect(matcher('/a/beta/c').matches).toBe(true);
    expect(matcher('/a/b/c').matches).toBe(true);
    expect(matcher('/a/b/ceta').matches).toBe(false);
  });
});

describe('matchPath', () => {
  it('handles empty trees', () => {
    const matcher = path => matchPath(createZipper([]), path);
    expect(matcher('/a/b')).toBeNull();
    expect(matcher('/a/beta/c')).toBeNull();
    expect(matcher('/a/b/c')).toBeNull();
  });
});
