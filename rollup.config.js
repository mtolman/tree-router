import { terser } from 'rollup-plugin-terser';
import typescript from '@rollup/plugin-typescript';
import resolve from '@rollup/plugin-node-resolve';
import babel from 'rollup-plugin-babel';

export default [
  {
    input: 'src/index.ts',
    output: [
      {
        file: 'dist/treeRouter.amd.js',
        format: 'amd'
      },
      {
        name: 'treeRouter',
        file: 'dist/treeRouter.umd.js',
        format: 'umd'
      },
      {
        file: 'dist/treeRouter.esm.js',
        format: 'es'
      },
      {
        file: 'dist/treeRouter.cjs.js',
        format: 'cjs'
      }
    ],
    plugins: [resolve(), typescript(), babel(), terser()]
  },
  {
    input: 'src/koa/index.ts',
    output: [
      {
        name: 'koaRouter',
        file: 'dist/koa.js',
        format: 'umd'
      },
      {
        file: 'dist/koa.esm.js',
        format: 'es'
      },
      {
        file: 'dist/koa.cjs.js',
        format: 'cjs'
      }
    ],
    plugins: [resolve(), typescript(), babel(), terser()]
  }
];
