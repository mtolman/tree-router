# Tree Router

Basic router based off of a tree datastructure.

Basic usage with Koa:

```
const Koa = require('koa')
const koaBody = require('koa-body')
const router = require('./router/koa')
const app = new Koa()

const routes = router([
  [
    '/add',
    {
      get: {
        parameters: { query: ['x', 'y'] },
        handler: ({ query: { x, y } }) => +x + +y
      },
      post: {
        handler: ({ body: { x, y } }) => +x + y,
        middleware: [
          (ctx, nxt) => {
            console.log(ctx.request.body)
            return nxt()
          }
        ]
      },
      middleware: [koaBody()]
    },
    [
      '/:x/:y',
      {
        any: {
          handler: ({
            pathParams: { x, y },
            query: { z = 0 },
            body: { q = 0 }
          }) => +x + +y + +z + +q
        }
      }
    ]
  ],
  ['/', 'hello']
])

app.use(routes)

app.listen(8080)
```
